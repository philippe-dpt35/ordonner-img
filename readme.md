Moteur javascript pour la création d'applications dont le but est d'ordonner des images par drag & drop.

Les noms des fichiers images à ordonner sont saisis dans un tableau au sein du fichier images.js:

```var items = [
  ["img1-1.jpg","img1-2.jpg","img1-3.jpg","img1-4.jpg","img1-5.jpg","img1-6.jpg","img1-7.jpg","img1-8.jpg"],
  ["insecte.png","souris.png","chat.png","chien.png","mouton.png","vache.png","elephant.png","baleine.png"]
  ];
  ```

  Il s'agit d'un tableau de tableaux dans lequel le 1er niveau correspond à une nouvelle liste, le second niveau aux images de la liste.
  Les images doivent être saisis dans l'ordre dans lequel elles doivent être ordonnées. Le script se chargera de les proposer dans un ordre aléatoire.
  Il n'y a pas de limites pour le nombre de listes, ni pour le nombre d'images de chaque liste.

  Pour créer une liste supplémentaire, ajouter une virgule à la fin du tableau de la ligne précédente, et ajouter le nouveau tableau avec ses éléments.

  Le script crée automatiquement les boutons de sélection de liste.

## Créer facilement ses listes d'images
Les listes d'images peuvent être créées beaucoup plus simplement grâce à la feuille de calcul Libreoffice creer-listes-img.ods.
Placez vos images dans le dossier img, puis ouvrez la feuille de calcul. Appuyez sur le bouton "Lister les images" pour en avoir la liste.
Faites un copier-coller du nom des images dans les lignes du tableau, indiquez par un nombre en pixels la hauteur des images de la série. Enfin, cliquez sur le bouton "Créer le fichier images.js". Celui-ci sera créé avec la syntaxe convenable.

Merci à zoom61 pour l'ajout de cette fonctionnalité.
