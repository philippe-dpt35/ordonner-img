"use strict"

const paramListes = {
  numeroListe: 0,
  nbListes: items.length,
  listesFaites: []
}

const paramPositionnement = {
  dernierSurvol: {},
  espaceDrop: 50, // Espace minimal laissé en fin de ligne pour permettre le drop
}

var boutons = [];
var solution = [];

var receptacle = document.getElementById("receptacle");

function alea(min,max) {
  let nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function melange(tableau) {
  let i;
  let tirage;
  let tab_alea = [];
  for (i = 0; i < tableau.length; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    } while (tableau[tirage] === 0);
    tab_alea[i] = tableau[tirage];
    tableau[tirage] = 0;
  }
  return tab_alea;
}

function creeBtListes() {
  let numero, i;
  const commandes = document.getElementById("commandes");
  const btVerif = document.getElementById("verifier");
  for (i = 0; i < paramListes.nbListes; i++) {
    const newBt = document.createElement("button");
    newBt.id = "liste" + i;
    newBt.type = "button";
    numero = i + 1;
    const t = document.createTextNode("liste" + numero);
    newBt.appendChild(t);
    newBt.onclick = defListe;
    boutons[i] = newBt;
    commandes.insertBefore(boutons[i], btVerif);
  }
}

function getIntValeur(nbPixels) {
  let valeur = parseInt(nbPixels.substr(0, nbPixels.length - 2));
  return valeur;
}

function creeListeItems() {
  let i;
  const reserve = document.getElementById("reserve");
  let listeItems = items[paramListes.numeroListe - 1];
  solution = items[paramListes.numeroListe - 1].join();
  listeItems = melange(listeItems);
  for (i = 0; i < listeItems.length; i++) {
    const newImg = new Image();
    newImg.id = "item" + i;
    newImg.src = "img/" + listeItems[i];
    newImg.style.maxHeight = hauteurImages[paramListes.numeroListe - 1];
    newImg.className = "collection";
    newImg.addEventListener("dragstart", drag);
    reserve.appendChild(newImg);
  }
  receptacle.style.minHeight = hauteurImages[paramListes.numeroListe - 1];
}

function desactiveBtListe() {
  let i;
  for (i=0; i<paramListes.nbListes; i++) {
    boutons[i].disabled = true;
  }
}

function activeBtListe() {
  let i;
  for (i=0; i<paramListes.nbListes; i++) {
    if (paramListes.listesFaites.indexOf(i+1)) {
      boutons[i].disabled = false;
    }
  }
}

function defListe() {
  paramListes.numeroListe = parseInt(this.innerText.substring(5));
  desactiveBtListe();
  // document.getElementById("commencer").disabled = false;
  commencer();
}

function getIdObjet(infosObjet) {
  let tableau = infosObjet.split("-");
  return tableau[0];
}

function getIdParent(infosObjet) {
  let tableau = infosObjet.split("-");
  return tableau[1];
}

function  reinitialise() {
  activeBtListe();
  while (receptacle.firstChild) {
    receptacle.removeChild(receptacle.firstChild);
  }
  paramListes.numeroListe = 0;
}

function commencer() {
  creeListeItems();
  document.getElementById("verifier").disabled = false;
}

function verification() {
  let i;
  let proposition = "";
  let position;
  for (i = 0; i < receptacle.childNodes.length; i++) {
    position = receptacle.childNodes[i].src.lastIndexOf("/") + 1;
    if (i + 1 < receptacle.childNodes.length) {
      proposition = proposition + receptacle.childNodes[i].src.substr(position) + ",";
      }
      else {
        proposition = proposition + receptacle.childNodes[i].src.substr(position);
      }
  }
  if (proposition !== solution) {
    return;
  }
  alert("Bravo !");
  paramListes.listesFaites.push(parseInt(paramListes.numeroListe));
  document.getElementById("verifier").disabled = true;
  reinitialise();
  if (paramListes.listesFaites.length === paramListes.nbListes) {
    alert("Félicitations ! Tu as réussi à classer correctement les " + paramListes.nbListes + " listes.");
  }
}

function resize() {
  // Si on dispose d'un espace suffisant pour le drop en fin de ligne, on supprime la ligne supplémentaire si elle existe
  if ((receptacle.lastChild) && (receptacle.lastChild.x + paramPositionnement.espaceDrop < receptacle.offsetWidth)) {
    if (getIntValeur(receptacle.style.minHeight) - getIntValeur(hauteurImages[paramListes.numeroListe - 1]) > 0 ) {
      receptacle.style.minHeight = getIntValeur(receptacle.style.minHeight) - getIntValeur(hauteurImages[paramListes.numeroListe - 1]) + "px";
    }
  }
  // Si l'espace en fin de ligne est insuffisant pour le drop, on ajoute une ligne
  if ((receptacle.lastChild) && (receptacle.lastChild.x + receptacle.lastChild.clientWidth + paramPositionnement.espaceDrop > receptacle.offsetWidth)) {
    receptacle.style.minHeight = getIntValeur(receptacle.style.minHeight) + getIntValeur(hauteurImages[paramListes.numeroListe - 1]) + "px";
  }
}
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
	let infosObjet;
	infosObjet = ev.target.id + "-" + ev.target.parentNode.id;
  ev.dataTransfer.setData("text", infosObjet);
}

function auSurvol(ev) {
  ev.preventDefault();
  let idObjet  = getIdObjet(ev.dataTransfer.getData('Text'));
  paramPositionnement.dernierSurvol = ev.target;
  if (ev.target.id === idObjet) {
    return;
  }
  var decalage = document.getElementById(idObjet).clientWidth;
  ev.target.style.paddingLeft = decalage + "px";
}

function finSurvol(ev) {
  paramPositionnement.dernierSurvol.style.paddingLeft = "0px";
}

function drop(ev) {
  ev.preventDefault();
  ev.target.style.paddingLeft = "0px";
  let idObjet  = getIdObjet(ev.dataTransfer.getData('Text'));
  let idParent = getIdParent(ev.dataTransfer.getData('Text'));
  const Objet = document.getElementById(idObjet);
  if (idParent === "reserve") {
    Objet.addEventListener("dragover", auSurvol);
    Objet.addEventListener("dragleave", finSurvol);
  }
  if (ev.target.id === "receptacle") {
    ev.target.appendChild(Objet);
    }
    else {
      if (Objet.className === "collection") {
        ev.target.parentNode.insertBefore(Objet, ev.target);
      }
  }
  // Si l'espace en fin de ligne est insuffisant pour le drop, on ajoute une ligne
  if ((receptacle.lastChild) && (receptacle.lastChild.x + Objet.clientWidth + paramPositionnement.espaceDrop > receptacle.offsetWidth)) {
    receptacle.style.minHeight = getIntValeur(receptacle.style.minHeight) + getIntValeur(hauteurImages[paramListes.numeroListe - 1]) + "px";
  }
  if (paramPositionnement.espaceDrop < parseInt(Objet.clientWidth / 2)) {
    paramPositionnement.espaceDrop = parseInt(Objet.clientWidth / 2);
  }
}

creeBtListes();
window.onresize = resize;