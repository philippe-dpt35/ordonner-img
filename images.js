/*
Entrer ici les noms des images à classer.
Chaque nouveau tableau correspond à une nouvelle liste.
Pour créer une nouvelle liste, il suffit d'ajouter un nouveau tableau 
sans oublier de mettre une vrigule à la fin de la ligne du tableau précédent.
Dans le tableau, les noms des images doivent être impérativement saisis dans 
l'ordre de classement. Le script se chargera de les proposer dans un ordre aléatoire.
Les fichiers images doivent être placés dans le dossier img
*/
var items = [
  ["img1-1.jpg","img1-2.jpg","img1-3.jpg","img1-4.jpg","img1-5.jpg","img1-6.jpg","img1-7.jpg","img1-8.jpg"],
  ["insecte.png","souris.png","chat.png","chien.png","mouton.png","vache.png","elephant.png","baleine.png"]
  ];
/*
Indiquer dans ce tableau la hauteur des images des différentes séries,
la première valeur correspondant à la pemière série,
la seconde à la deuxième série, etc.
*/
var hauteurImages = ["145px","100px"];
